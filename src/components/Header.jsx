import React from 'react';

import {Navbar, Nav, Form, FormControl, Button, Glyphicon} from 'react-bootstrap';

class Header extends React.Component {
  render() {
    return (
      <Navbar fixed="top">
        <Navbar.Brand href="#home">
          <img
            alt=""
            src="/logo.svg"
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{' '}
          Our Awesome Store
        </Navbar.Brand>
        <Navbar.Toggle onClick={this.toggleCollapse} />
        <Navbar.Collapse>
          <Nav className="mr-auto">
            <Nav.Item>
              <Nav.Link href="#home">Home</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="#features">Features</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="#pricing">Pricing</Nav.Link>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-primary">Search</Button>
        </Form>
        {/* <Glyphicon glyph="shopping-cart" /> */}
      </Navbar>
    );
  }
}


export default Header;
